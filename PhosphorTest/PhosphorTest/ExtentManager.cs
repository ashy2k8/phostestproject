﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhosphorTest
{
    public class ExtentManager
    {
        static ExtentReports extent;

        public static ExtentReports GetIntance()
        {
            return extent;
        }

        public static ExtentReports CreateInstance(string filename)
        {
            filename = @"C: \Users\Ashnil\Desktop\PhosphorTest - Copy\PhosphorTest\bin\Debug\Reports\extent.html";
            ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(filename);

            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);

            return extent;
        }
    }
}
