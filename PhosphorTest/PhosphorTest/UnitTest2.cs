﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using com.sun.corba.se.spi.activation;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using sun.net.www.http;

namespace PhosphorTest
{
    [TestClass]
    public class UnitTest1
    {
        IWebDriver driver;
        string baseUrl = "http://bauhaus.stagephos.co.nz";
        BasicReport basicReport;

        public ExtentTest test;
        public ExtentReports extent;

        [TestInitialize]
        public void TestInitialize()
        {
            driver = new ChromeDriver();
            driver.Url = baseUrl;
            driver.Manage().Window.Maximize();
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3000);

            // start reporters
            var htmlReporter = new ExtentHtmlReporter(@"C:\Users\Ashnil\Desktop\PhosphorTest - Copy\PhosphorTest\bin\Debug\Reports\extent.html");
            htmlReporter.AppendExisting = false;
            // create ExtentReports and attach reporter(s)
            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
          

             // creates a toggle for the given test, adds all log events under it    
             test = extent.CreateTest("Check Fav Icon", "Sample description");
           

            // log(Status, details)
            //test.Log(Status.Info, "This step shows usage of log(status, details)");

            // info(details)
            //test.Info("This step shows usage of info(details)");

            // log with snapshot
            //test.Fail("details", MediaEntityBuilder.CreateScreenCaptureFromPath("screenshot.png").Build());

            // test with snapshot
            //test.AddScreenCaptureFromPath("screenshot.png");

            

        }  

        [TestMethod]
        public void CheckFavIcon()
        {

            List<string> favIconRelToCheck = new List<string>
            {
                "//*[@rel = 'shortcut icon']",
                "//*[@rel = 'icon']"
            };

            var favIconFound = 0;
            
            foreach(var rels in favIconRelToCheck)
            {
                favIconFound += driver.FindElements(By.XPath(rels)).Count;
            }

            if (favIconFound == 0)
            {
                //basicReport.extent.CreateTest("MyFirstTest", "Test Description").Pass("details");
                test.Fail("No Fav Icon Found");
                 
                Assert.Fail();
            }
            else
            {
                test.Pass("Fav Icon Found");
            }

        }

        [TestMethod]
        public void CheckMetaTagViewPort()
        {
            var viewPort = driver.FindElement(By.XPath("//meta[@name='viewport']")).GetAttribute("content");
            Assert.AreEqual("width=device-width, initial-scale=1.0", viewPort);
        }

        [TestMethod]
        public void CheckTitle()
        {
            string siteTitle = driver.Title;

            if (siteTitle.Length < 2)
            {
                
                Assert.Fail();
            }
            else
            {
                
            }

        }

        [TestMethod]
        public void CheckIfGoogleAnalyticsIsEntered()
        {
            var scripts = driver.FindElements(By.TagName("script"));
            var isGoogleAnalyticsEntered = false;

            for (var i = 0; i < scripts.Count; i++)
            {
                var script = scripts[i].GetAttribute("src");
                if (script.Contains("www.google-analytics.com/analytics.js") || script.Contains("www.googletagmanager.com/gtm.js"))
                {
                    isGoogleAnalyticsEntered = true; ;
                }

            }

            if (!isGoogleAnalyticsEntered)
            {
                 
                Assert.Fail();
            }
            else
            {
                 
            } 
        }

        [TestMethod]
        public void CheckJavascriptErrors()
        {
            var javascriptErrors = driver.Manage().Logs.GetLog(LogType.Browser);
            if(javascriptErrors.Count > 0)
            {
                Assert.Fail();
            }
        }

        //[TestMethod]
        //public void CheckIfHtmlIsValid()
        //{
        //    var client = new RestClient("https://api.greenplan.co.nz/");
        //    var request2 = new RestRequest("api/GetInvestorProfile", Method.GET);

        //}

        [TestMethod]
        public void CheckIfCassetteIsBundling()
        {
            var everypageCount = 0;
            var scripts = driver.FindElements(By.TagName("script"));

            for(var i = 0; i < scripts.Count; i++)
            {
                var script = scripts[i].GetAttribute("src");

                if (script.ToLower().Contains("everypage"))
                {
                    everypageCount += 1;
                }
            }

            if(everypageCount > 1)
            {
                Assert.Fail();
            }
            
        }

        [TestMethod]
        public void CheckAllWebsiteLinks()
        {
            WebClient webClient = new WebClient();
            var linkToXml = baseUrl + "/sitemap.xml";

            XmlTextReader reader = new XmlTextReader(linkToXml);
            var doc = new XmlDocument();
            doc.Load(linkToXml);

            XmlElement root = doc.DocumentElement;
            XmlNodeList nodes = root.GetElementsByTagName("url");

            //List<string> brokenUrls = new List<string>();

            foreach(XmlNode node in nodes)
            {
                var pageUrl = node["loc"].InnerText;
                try
                {
                    var robotsTxtContents = (new WebClient().DownloadString(pageUrl));
                }
                catch (WebException ex)
                {
                    Assert.Fail();
                }
            }
        }

        [TestMethod]
        public void CheckKeywords()
        {
            string keywords = driver.FindElement(By.XPath("//meta[@name='keywords']")).GetAttribute("content");
            if (keywords.Length < 2)
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        public void CheckDescription()
        {
            var siteDescription = driver.FindElement(By.XPath("//meta[@name='description']")).GetAttribute("content");
            
            if (siteDescription.Length < 2)
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        public void CheckRobotsTxtFile()
        {
            var Url = baseUrl + "/robots.txt";

            var robotsTxtContents = (new WebClient().DownloadString(Url));

            robotsTxtContents = robotsTxtContents.Replace(Environment.NewLine, string.Empty);

            List<string> textsToCheck = new List<string>
            {
                "User-agent: *",
                baseUrl + "/sitemap.xml",
                "Disallow: /umbraco",
                "Disallow: /umbraco_client"
            };

            foreach(var textToCheck in textsToCheck)
            {
                CheckRobotsTextContentsStringContainText(robotsTxtContents, textToCheck);
            }
            
        }

        [TestMethod]
        public void CheckIf404PageExsits()
        {
            var url = baseUrl + "/404";

            try
            {
                var pageToCheck = (new WebClient().DownloadData(url));

            }
            catch (WebException ex)
            {

                Stream receiveStream = ex.Response.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(receiveStream, encode);
                var pageContent = readStream.ReadToEnd();

                if(pageContent.Contains("The resource you are looking for has been removed, had its name changed, or is temporarily unavailable."))
                {
                    Assert.Fail();

                }else if (pageContent.Contains("The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable."))
                {
                    Assert.Fail();
                }
                else if (pageContent.Contains("No umbraco document matches the url"))
                {
                    Assert.Fail();
                }


            }

        }

        [TestMethod]
        public void ValidateHtmlSyntax()
        {
            var web = new HtmlWeb();
            


            WebClient webClient = new WebClient();
            var linkToXml = baseUrl + "/sitemap.xml";
            

            XmlTextReader reader = new XmlTextReader(linkToXml);
            var doc = new XmlDocument();
            doc.Load(linkToXml);

            XmlElement root = doc.DocumentElement;
            XmlNodeList nodes = root.GetElementsByTagName("url");

            List<string> brokenUrls = new List<string>();

            foreach (XmlNode node in nodes)
            {
                var pageUrl = node["loc"].InnerText;
                try
                {
                    var htmlSyntaxdoc = web.Load(pageUrl);

                    if (htmlSyntaxdoc.ParseErrors.Count() > 0)
                    {
                        //Assert.Fail();
                        brokenUrls.Add(pageUrl);
                    }
                }
                catch (WebException ex)
                {
                    Assert.Fail();
                }
            }

        }

        //[TestMethod]
        //public void CheckIfCacheHeaderSet()
        //{

        //    var client = new WebClient();
        //    client.DownloadData("https://cape.org.nz");
        //    var alalal = client.ResponseHeaders.Get("cache-control");

        //}

        [TestMethod]
        public void CheckIf500PageExists()
        {
            var url = baseUrl + "/error.html";

            var pageToCheck = (new WebClient().DownloadString(url));

            if(!pageToCheck.Contains("Oops, Something went wrong! (500)"))
            {
                test.Log(Status.Fail, "No 500 Page Exists");
                Assert.Fail();
            }
            else
            {
                test.Log(Status.Pass, "500 Page Found");
            }
        }

        //[TestMethod]
        //public void CheckSiteSpeed()
        //{
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        //}

        [TestCleanup]
        public void CleanUp()
        {
            extent.Flush();
            driver.Quit();
        }

        private void CheckRobotsTextContentsStringContainText(string robotsTextContents, string textToCheck)
        {
            if (!robotsTextContents.Contains(textToCheck))
            {
                // calling flush writes everything to the log file
                Assert.Fail();
            }
        }
    }
}